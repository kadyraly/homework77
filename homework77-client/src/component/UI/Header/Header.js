import React from 'react';
import './Header.css';
import {NavLink} from "react-router-dom";

const Header = props => {
    return (
        <div className="Header">
            <ul>
                <li><NavLink activeClassName="selected" exact to="/">Chats</NavLink></li>
                <li><NavLink activeClassName="selected" to="/chats/new">New Chat</NavLink></li>
            </ul>
        </div>
    )
};

export  default Header;