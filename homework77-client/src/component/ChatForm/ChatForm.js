import React, {Component} from 'react';
import './ChatForm.css';

class ChatForm extends Component {
    state = {
        author: '',
        message: '',
        image: ''
    };

    submitFormHandler = event => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {

        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    render() {
        return (
            <form action="" className="ChatForm" onSubmit={this.submitFormHandler}>
                <div>
                    <label>Author:</label>
                    <input
                        type="text"
                        value={this.state.author}
                        placeholder="author here..."
                        className="form-control"
                        name="author"
                        onChange={this.inputChangeHandler}
                    />
                </div>
                <div>
                    <label>Message:</label>
                    <textarea
                        name="message"
                        value={this.state.message}
                        placeholder="message"
                        className="form-control"
                        onChange ={this.inputChangeHandler}
                        required
                    />
                </div>
                <div>
                    <label>Image:</label>
                    <input
                        type="file"
                        name="image"
                        onChange ={this.fileChangeHandler}
                    />
                </div>

                <div>
                    <button type="submit">Send</button>
                </div>
            </form>
        );
    }
}

export default ChatForm;
