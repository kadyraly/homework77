import {FETCH_CHATS_SUCCESS} from "../action/actionTypes";

const initialState = {
    chats: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_CHATS_SUCCESS:
            return {...state, chats: action.chats};
        default:
            return state;
    }
};
export default reducer;