import  React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {createChat} from "../../store/action/chat";
import ChatForm from "../../component/ChatForm/ChatForm";


class NewChat extends Component {

    createChat = chatData => {
        this.props.onChatCreated(chatData).then(() => {
            this.props.history.push('/');
        });
    };
    render() {
        return (
            <Fragment>
                <ChatForm onSubmit={this.createChat} />
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onChatCreated: chatData => dispatch(createChat(chatData))
    }
};

export default connect(null, mapDispatchToProps)(NewChat);